#!/usr/bin/env python
# A node to convert ByteMultiArray (published by the pr2_ethercat realtime control loop) to Efield sensor data
# Derived from publisher created by Liang-Ting Jiang
# By Patrick Lancaster

import rospy
import array
import math
from std_msgs.msg import ByteMultiArray
from pr2_pretouch_efield.msg import EField
from math import *

ts = [0, 0, 0, 0]
seq = 0
efield = EField()
buf = array.array('i',(0 for i in range(0,2)))
inPhaseBuf = array.array('i', (0 for i in range(0,4)))
quadBuf = array.array('i', (0 for i in range (0,4)))
idx = 0

rospy.init_node("efield_publisher", anonymous=True)
side = rospy.get_param('~side', 'right')
rospy.loginfo("Started the efield_publisher for the %s gripper", side)
if side == 'right':
  sub_topic = 'raw_pressure/r_gripper_motor'
elif side == 'left':
  sub_topic = 'raw_pressure/l_gripper_motor'
else:
  print side + ' is not a valid side parameter (only right/left supported)'
  raise

pub_topic = 'efield/' + side
pub = rospy.Publisher(pub_topic, EField)

#write data to a .txt file for future analysis
def write_A_Row_To_File(data1, data2, data3, data4, fileName):
	#inPhaseTemp, quadTemp, mag, phase
	File = open(fileName, 'w') #open the file in the write mode
	File.write(str(data1))
	File.write('  ')
	File.write(str(data2))
	File.write('  ')
	File.write(str(data3))
	File.write('  ')
	File.write(str(data4))
	File.write('\n')
	File.close() #close the file

#callback function
def rawPressureCallback(msg):
  global seq, ts, efield, idx, buf
  #only publish if the timestamp is different from the last message
  #this is for removing the duplicated messages
  if msg.data[:4] != ts: # and msg.data[4] > 10:
#    val1 = (256+msg.data[4]) if msg.data[4] <0 else msg.data[4]
#    val2 = (256+buf[0]) if buf[0] <0 else buf[0]
#    val3 = (256+msg.data[6]) if msg.data[6]<0 else msg.data[6]
#    val4 = (256+buf[1]) if buf[1] <0 else buf[1]

    val1 = msg.data[4]
    val2 = buf[0]
    val3 = msg.data[6]
    val4 = buf[1]


    buf[0] = msg.data[5]
    buf[1] = msg.data[7]

#    val1 = (256+msg.data[4]) if msg.data[4]<0 else msg.data[4]
#    val2 = (256+msg.data[5]) if msg.data[5]<0 else msg.data[5]
#    val3 = (256+msg.data[6]) if msg.data[6]<0 else msg.data[6]
#    val4 = (256+msg.data[7]) if msg.data[7]<0 else msg.data[7]

    seq += 1
    ts = msg.data[:4]
    efield.header.seq = seq
    efield.header.stamp = rospy.Time.now()
#    print "Data = %s %s %s %s" % (str(val1), str(val2), str(val3), str(val4))    
    if val2 < 0:
        val2 += 256
    if val4 < 0:
        val4 += 256

    inPhase = (((val1) << 8) + (val2 & 0x00ff)) # Labels verified
    quad = (((val3) << 8) + (val4 & 0x00ff) )
    inPhaseBuf[idx] = inPhase
    quadBuf[idx] = quad
    inPhaseTemp = int(sum(inPhaseBuf) / 4)
    quadTemp = int(sum(quadBuf) / 4)
    mag = sqrt(inPhaseTemp**2 + quadTemp**2)
    phase = 0
    if quadTemp != 0:
        phase = atan(float(inPhaseTemp)/quadTemp)
    elif inPhaseTemp > 0:
        phase = math.pi/2
    elif inPhaseTemp < 0:
        phase = -1.0*math.pi/2
    
    #write data to a file
    file_name = 'datalog1_7_7.txt'
    write_A_Row_To_File(inPhaseTemp, quadTemp, mag, phase, target)
    
    efield.data = [inPhaseTemp, quadTemp, mag, phase]
    pub.publish(efield)
#    print "Data = %s %s %s %s" % (str(val1), str(val2), str(val3), str(val4))
    idx = (idx + 1) % 4

sub = rospy.Subscriber(sub_topic, ByteMultiArray, rawPressureCallback)
rospy.spin()

