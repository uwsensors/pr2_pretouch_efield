#!/usr/bin/env python
#This node subsribes from a topic to get the smoothed data points
#and basing on the smoothed data points to make corresponding moving
#decisions. This is the core part of the finger follow demo since it
#has all the parts that make decisions basing on the sensor data.
#2 modes: (Put the index finger close to the E-field sensor very QUICKLY
# will start the system to get it from the Stop mode to Move mode. Pull
# the finger away from the E-field sensor very QUICKY will make the system
# go from the Move mode to the Stop mode in which the gripper is not moving).
#Mode 1: Stop mode
#Mode 2: Move mode
#By Adrian and Lincong Li 7/19


#headers
import rospy
import array
import math
#from std_msgs.msg import ByteMultiArray
from pr2_pretouch_efield.msg import EField
from std_msgs.msg import String
from math import *

import numpy

# Importing service file
from pr2_move.srv import *


# MOVING CLIENT PART
#####################################################################
def move_gripper_client(side, position, effort, async):
	if(position == -1.0): #stopppp
		rospy.wait_for_service('stop_gripper')
		try:
			stop_gripper = rospy.ServiceProxy('move_gripper', SimpleGripperMove)
			stop_gripper(side)
		except rospy.ServiceException, e:
			message = "Failed calling stop service"
			rospy.loginfo(message)
		
	else:
		rospy.wait_for_service('move_gripper')
		try:
			move_gripper = rospy.ServiceProxy('move_gripper', GripperMove)
			move_gripper(side, position, effort, async)
		except rospy.ServiceException, e:
			message = "Failed calling move service"
			rospy.loginfo(message)


def decision_make(var):
	global initial_pos
	global moving_closer
	global global_Count 
	global stable
	
	#global moving
	nonBlocking = True
	unit_Move = 0.0035
	frequency_Scaler = 7
	higher_Bound_Sensor_Val = -1800
	lower_Bound_Sensor_Val = -1900

	if(global_Count % frequency_Scaler == 0): #only choose 1 out of frequency_Scaler number of data points
		if (var < lower_Bound_Sensor_Val): #moving closer
			stable = 0
			if(moving_closer == 0): #if last time it's not moving closer, stop the movement
				move_gripper_client('left', -1, -1, nonBlocking) #stop
				moving_closer=1
			else:	
				initial_pos -= unit_Move
				rospy.loginfo('Moved closer a little!' + str(initial_pos))
				move_gripper_client('left', initial_pos, -1, nonBlocking)
			rospy.loginfo('Finished')	
	
		elif(var > higher_Bound_Sensor_Val): 
			stable = 0
			if(moving_closer == 1): #if last time it's not moving closer, stop the movement
				move_gripper_client('left', -1, -1, nonBlocking) #stop
				moving_closer=0
			
			else:
				initial_pos += unit_Move
				rospy.loginfo('Moved further a little!' + str(initial_pos))
				move_gripper_client('left', initial_pos, -1, nonBlocking)
			rospy.loginfo('Finished')
		else:	
			if(stable == 0):
				move_gripper_client('left', -1, -1, nonBlocking) #stop
				stable=1
			rospy.loginfo("Current position is:" + str(initial_pos))
			rospy.loginfo('Stable')
######################################################################

class data_Pt: 
	def __init__(self, time, y_Val): #constructor
		self.t = time
		self.y = y_Val

	#def __del__(self):
		#print 'Object deleted'

	def Print(self):
		print 'Start time: ' + str(self.t)
		print 'Start val: ' + str(self.y)

#Inpit argument: 2 point objects
#Return: distance between 2 point objects
def calculate_Distance(pt1, pt2):
	result = sqrt(pow((pt1.t - pt2.t), 2) + pow((pt1.y - pt2.y), 2))
	return result


#This function compares the distance from 2 points to the threshhold line
#Input argument: 
#2 point objects
#a threshhold value
#Return:
#'p' ==> previous point is closer to the threshhold
#'c' ==> current point is closer to the threshhold
#helper function
def compare_Distance(pre_Pt, cur_Pt, threshhold):
	dist_Previous = 0
	dist_Current = 0
	cross_Pt_t = 0
	cross_Pt_y = threshhold
	#calculate parameters for linear function
	k = 0
	b = 0
	if(cur_Pt.y == pre_Pt.y):
		return 'c'
	k = (cur_Pt.y - pre_Pt.y)/float( (cur_Pt.t - pre_Pt.t) )
	b = cur_Pt.y - k*(cur_Pt.t)
	cross_Pt_t = (cross_Pt_y - b)/k
	cross_Pt = data_Pt(cross_Pt_t, cross_Pt_y)
	#calculate distances
	dist_Previous = calculate_Distance(pre_Pt, cross_Pt)#sqrt(pow((pre_Pt), 2) + pow((), 2))
	dist_Current = calculate_Distance(cur_Pt, cross_Pt)
	if(dist_Previous < dist_Current):
		return 'p'
	else:
		return 'c'

#Return the slope as a float
def calculate_Slope(y1, y2, delta_X):
	scale_Factor = float(delta_X);
	return ((float(y2)-float(y1))/scale_Factor)

def calculate_Raising_Slope(var, buffer_Pt, start_Pt, end_Pt):
	global rasing_Slope_Threshhold
	global higher_Threshhold
	global lower_Threshhold
	global R_start_Detected
	global global_Count
	var = float(var)
	global_Count = global_Count + 1 #increment the global count

	slope_Calculated = 0

	#case 1:
	#never detect start point before
	if( (var > lower_Threshhold ) and (R_start_Detected == 0) and (global_Count > 1)):

	
		R_start_Detected = 1
		start_Pt.t = global_Count
		start_Pt.y = var

		#make decision which one to use, previous one or the current one
		if('p' == compare_Distance(buffer_Pt, start_Pt, lower_Threshhold)): #if the previous is a better choice
			#copy the information from the previous point to the current point
			start_Pt.t = buffer_Pt.t
			start_Pt.y = buffer_Pt.y

		buffer_Pt.t = global_Count
		buffer_Pt.y = var
		return 'Not_yet'

	#case 2:
	#already detect the start point but now it falls back below the low level
	if( (var <= lower_Threshhold) and (R_start_Detected == 1) ):
		#imessage = "Rasing above high threshhold!!!"
                #rospy.loginfo(message)

		R_start_Detected = 0

		buffer_Pt.t = global_Count
		buffer_Pt.y = var
		return 'Not_yet'

	#case 3:
	#detected start point before and now detected end point
	if((var >= higher_Threshhold ) and (R_start_Detected == 1) ):

		end_Pt.t = global_Count
		end_Pt.y = var
		if('p' == compare_Distance(buffer_Pt, end_Pt, higher_Threshhold)): #if the previous is a better choice
			end_Pt.t = buffer_Pt.t
			end_Pt.y = buffer_Pt.y

		slope_Calculated = calculate_Slope(start_Pt.y, end_Pt.y, (end_Pt.t - start_Pt.t))
		buffer_Pt.t = global_Count
		buffer_Pt.y = var
		#rospy.loginfo(slope_Calculated)	
		if(slope_Calculated >= rasing_Slope_Threshhold):
			return 'start'
		else:
			return 'Not_yet'

	buffer_Pt.t = global_Count
	buffer_Pt.y = var
	return 'Not_yet'


#---------------------------------------------------------------
def calculate_Falling_Slope(var, buffer_Pt, start_Pt, end_Pt):
	global falling_Slope_Threshhold
	global higher_Threshhold
	global lower_Threshhold
	global F_start_Detected
	global global_Count
	global_Count = global_Count + 1 #increment the global count
	slope_Calculated = 0

	#case 1:
	#never detect start point before
	if( (var <= higher_Threshhold ) and (F_start_Detected == 0)):
		F_start_Detected = 1

		start_Pt.t = global_Count
		start_Pt.y = var
		#make decision which one to use, previous one or the current one
		if('p' == compare_Distance(buffer_Pt, start_Pt, higher_Threshhold)): #if the previous is a better choice
			start_Pt.t = buffer_Pt.t
			start_Pt.y = buffer_Pt.y
		#start_Pt.Print()
		buffer_Pt.t = global_Count
		buffer_Pt.y = var
		return 'Not_yet'


	#case 2:
	#already detect the start point but now it raise back below the high level
	if((var >= higher_Threshhold) and (F_start_Detected == 1)):
		F_start_Detected = 0

		buffer_Pt.t = global_Count
		buffer_Pt.y = var
		return 'Not_yet'

	#case 3:
	#detected start point before and now detected end point
	if((var < lower_Threshhold ) and (F_start_Detected == 1)): #and (i != start_Pt)):

		end_Pt.t = global_Count
		end_Pt.y = var
		#make decision which one to use, previous one or the current one
		if('p' == compare_Distance(buffer_Pt, end_Pt, lower_Threshhold)): #if the previous is a better choice
			end_Pt.t = buffer_Pt.t
			end_Pt.y = buffer_Pt.y
		
		slope_Calculated = calculate_Slope(start_Pt.y, end_Pt.y, (end_Pt.t - start_Pt.t))
		buffer_Pt.t = global_Count
		buffer_Pt.y = var

		if(slope_Calculated <= falling_Slope_Threshhold):
			return 'stop'
		else:
			return 'Not_yet'

	buffer_Pt.t = global_Count
	buffer_Pt.y = var
	return 'Not_yet'

#---------------------------------------------------------------
efield = EField() #create an efield object
#initially the gripper is not moving
moving_closer = 0
stable = 0

pause_State = 1
move_State = 0
rasing_Slope_Threshhold = 700.0
falling_Slope_Threshhold = -650.0
higher_Threshhold = -1150.0
lower_Threshhold = -2500.0
initial_pos = 0.085
#points to keep track of the raising phase
R_start_Pt = data_Pt(1,1)
R_end_Pt = data_Pt(1,1)
R_start_Detected = 0
#points to keep track of the falling phase
F_start_Pt = data_Pt(0,0)
F_end_Pt = data_Pt(0,0)
F_start_Detected = 0

buffer_Pt = data_Pt(0,0)
global_Count = 0
#node and topic initialization
rospy.init_node("efield_Mover", anonymous=True)

#call back function
#-----------------------------------------------
def moveCallback(msg):
	global efield
	global initial_pos
	global pause_State
	#global move_State
	global rasing_Slope_Threshhold
	global falling_Slope_Threshhold
	#global points
	global R_start_Pt
	global R_end_Pt
	global F_start_Pt
	global F_end_Pt
	global buffer_Pt
	global R_start_Detected
	global F_start_Detected
	global higher_Threshhold
	global lower_Threshhold
	global global_Count

	raw_var = msg.data[0]
	var = float(raw_var)
	if(pause_State == 1): #pausing mode
		#rospy.loginfo("waiting")
		if('start' == calculate_Raising_Slope(var, buffer_Pt, R_start_Pt, R_end_Pt)):
			pause_State = 0
			message = "started!!!" #signaling the start
			rospy.loginfo(message)
	
	else: #moving mode
		decision_make(var) #make decisions basing on the current data point value
			
		if('stop' == calculate_Falling_Slope(var, buffer_Pt, R_start_Pt, R_end_Pt)):
			pause_State = 1
			message = "stopped!!!" #signaling the end
			#empty_Sapce = " "
			rospy.loginfo(message)

#-----------------------------------------------

sub_topic = 'efield/smoothed_Data'
initial_pos = 0.085
#where to subsribe, what is the type and callback function
sub = rospy.Subscriber(sub_topic, EField, moveCallback)
rospy.spin()	
