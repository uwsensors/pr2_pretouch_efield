#include "pr2_pretouch_efield/fabfish_pub.h"

FabfishPub::FabfishPub(ros::NodeHandle& node, std::string& side) : pub_topic_("/fabfish/"+side), sub_topic_("/raw_pressure/"+side.substr(0,1)+"_gripper_motor"),pub_(node.advertise<pr2_pretouch_efield::EField>(pub_topic_, 1000)), sub_(node.subscribe(sub_topic_, 1000, &FabfishPub::dataCallback, this)), ts_(4,0), seq_(0) {

}

FabfishPub::~FabfishPub() {

}

void FabfishPub::dataCallback(const std_msgs::ByteMultiArray& msg) {

	if(msg.data[0] == ts_[0] && msg.data[1] == ts_[1] && msg.data[2] == ts_[2] && msg.data[3] == ts_[3]) {
		return;
	}

	// Set timestamp
	ts_[0] = msg.data[0];
	ts_[1] = msg.data[1];
	ts_[2] = msg.data[2];
	ts_[3] = msg.data[3];

	data_.header.seq = seq_;
	data_.header.stamp = ros::Time::now();

	// msg.data[4] is junk
	int16_t inphase = 0x0;
	int16_t quad = 0x0;
	
	for(int i = 0; i < 2; i++) {
		inphase |= (0x00FF&msg.data[i+5]) << (8*i);
		quad |= (0x00FF&msg.data[i+7]) << (8*i);
	}

	double mag = std::sqrt(std::pow(inphase,2)+std::pow(quad,2));
	double phase = std::atan2(inphase, quad);
	data_.lData.resize(4);
	data_.lData[0] = inphase;
	data_.lData[1] = quad;
	data_.lData[2] = mag;
	data_.lData[3] = phase;

	pub_.publish(data_);
	
	ROS_INFO("%d %d %d %d %d %d %d %d %d",msg.data[4], msg.data[5], msg.data[6], msg.data[7], msg.data[8], msg.data[9], msg.data[10], msg.data[11], msg.data[12]);

	seq_++;
}

int main(int argc, char** argv){

	ros::init(argc, argv, "fabfish_pub");
	ros::NodeHandle node;

	std::string side("right");
	
	if(argc >= 2) {
		side = argv[1];
	}

	FabfishPub fish_pub(node,side);

	ros::spin();
}
