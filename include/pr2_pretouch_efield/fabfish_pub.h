#ifndef FABFISH_PUB_H
#define FABFISH_PUB_H

#include "ros/ros.h"
#include "pr2_pretouch_efield/EField.h"
#include "std_msgs/ByteMultiArray.h"
#include <vector>

class FabfishPub {

public:
	FabfishPub(ros::NodeHandle& node, std::string& side);
	~FabfishPub();
	void dataCallback(const std_msgs::ByteMultiArray& msg);

private:
	std::string pub_topic_;
  std::string sub_topic_;
	ros::Publisher pub_;
	ros::Subscriber sub_;
	std::vector<int> ts_;
	pr2_pretouch_efield::EField data_;
	int seq_;
};

#endif
