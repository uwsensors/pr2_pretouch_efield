#!/usr/bin/env python
#By Adrian and Lincong Li 7_9

#headers
import rospy
import array
import math
#from std_msgs.msg import ByteMultiArray
from pr2_pretouch_efield.msg import EField
from math import *

import numpy

#functions
#-------------------------------------------
def average(arr):
		Sum = 0
		length = len(arr)
		ave = 0
		for i in range(0, length):
			Sum = Sum + float(arr[i])
		ave = Sum/length
		return ave

def write_A_Row_To_File(data1, data2, data3, data4, file):
	
	fileName = open(file, 'a')
	fileName.write(str(data1))
	fileName.write('  ')
	fileName.write(str(data2))
	fileName.write('  ')
	fileName.write(str(data3))
	fileName.write('  ')
	fileName.write(str(data4))
	fileName.write('\n')
	fileName.close()
#-------------------------------------------s

#variable
efield = EField() #create an efield object
unit_Num = 3 #temporary number of data points for each group
count_dps = 0  #data points counter

data_1 = []
data_2 = []
data_3 = []
data_4 = []
buf = [data_1, data_2, data_3, data_4] #holds unit_Num sample data points

#node and topic initialization
rospy.init_node("efield_data_smoother", anonymous=True)
pub_topic = 'efield/smoothed_Data' #define a new topic
pub = rospy.Publisher(pub_topic, EField) #where to publish and what to publish

#call back function
#-----------------------------------------------
def smoothedCallback(msg):
	global efield, unit_Num, count_dps, buf

	if(count_dps == unit_Num): #when the buffer is full
		result_1 = average(buf[0])
		result_2 = average(buf[1])
		result_3 = average(buf[2])
		result_4 = average(buf[3])
		#writing results to a file for testing purpose
		file_Name = '/home/map34/efield_ws/src/pr2_pretouch_efield/src/smooth_Node_Output_7_13'
		write_A_Row_To_File(result_1, result_2, result_3, result_4, file_Name)
		efield.data = [result_1, result_2, result_3, result_4]
		pub.publish(efield)
		#rest
		count_dps = 0
		buf[0] = []
		buf[1] = []
		buf[2] = []
		buf[3] = []

	#efield.data = [inPhaseTemp, quadTemp, mag, phase]

	#fill the buffer with input data
	for z in range(0, 4):
		buf[z].append(msg.data[z])

	count_dps = count_dps+1 #update the counter
#-----------------------------------------------

sub_topic = 'efield/left'
#where to subsribe, what is the type and callback function
sub = rospy.Subscriber(sub_topic, EField, smoothedCallback)
rospy.spin()
