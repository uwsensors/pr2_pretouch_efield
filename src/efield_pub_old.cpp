#include <stdio.h>
#include <stdlib.h>
#include <ros/ros.h>
#include "pr2_pretouch_efield/EField.h"
#include <std_msgs/ByteMultiArray.h>
#include <string>
#include <iostream>
#include <fstream>
#include <iomanip>
#include <vector>

using namespace std;
#define BUFFER_SIZE 4

ros::Publisher* pubPtr;
std::vector<int> ts(4);
pr2_pretouch_efield::EField data;
std::vector<float> lInPhaseBuf(BUFFER_SIZE);
std::vector<float> rInPhaseBuf(BUFFER_SIZE);
std::vector<float> lQuadBuf(BUFFER_SIZE);
std::vector<float> rQuadBuf(BUFFER_SIZE);

int seq;
float lInPhase;
float lQuad;
double lMag;
double lPhase;
float rInPhase;
float rQuad;
double rMag;
double rPhase;
bool first;
string numFile;

void write_A_Row_To_File(float data1, float data2, float data3, float data4, const char* fileName) {
	//inPhase, quad, mag, phase
	ofstream file;
	file.open(fileName, ios::app); //open file
	//write data to the file
	file << fixed << setprecision(5) << data1 <<" "<< data2 <<" "<< data3<<" "<< data4 << endl;
	file.close(); //close file
}


void rawDataCallback(const std_msgs::ByteMultiArray msg) {
	// Check time stamp
	if(msg.data[0] == ts[0] && msg.data[1] == ts[1] && msg.data[2] == ts[2] && msg.data[3] == ts[3]) {
		return;
	}
	// Set timestamp
	ts[0] = msg.data[0];
	ts[1] = msg.data[1];
	ts[2] = msg.data[2];
	ts[3] = msg.data[3];

	// Prepare message that is to be published
	data.header.seq = seq;
	data.header.stamp = ros::Time::now();
	
	if((msg.data[6] < 0 && msg.data[5] <= 0) || (msg.data[6] >= 0 && msg.data[5] >= 0)) {
		lInPhase = 256*msg.data[6]+msg.data[5];
	} else if(msg.data[6] < 0 && msg.data[5] >= 0) {
		lInPhase = 256*msg.data[6]+msg.data[5]-256;
	} else {
		lInPhase = 256*msg.data[6]+msg.data[5]+256;
	}

	if((msg.data[8] < 0 && msg.data[7] <= 0) || (msg.data[8] >= 0 && msg.data[7] >= 0)){
		lQuad = 256*msg.data[8]+msg.data[7];
	} else if(msg.data[8] < 0 && msg.data[7] >= 0)
		lQuad = 256*msg.data[8]+msg.data[7]-256;
	else {
		lQuad = 256*msg.data[8]+msg.data[7]+256;
	}
//ROS_INFO("%d %d %d %d %d %d %d %d %d %d", msg.data[4], msg.data[5], msg.data[6], msg.data[7], msg.data[8], msg.data[9],msg.data[10],msg.data[11],msg.data[12],msg.data[13]);
	if(msg.data.size() >= 14) {	

		if((msg.data[11] < 0 && msg.data[10] <= 0) || (msg.data[11] >= 0 && msg.data[10] >= 0)) {
			rInPhase = 256*msg.data[11]+msg.data[10];
		} else if(msg.data[11] < 0 && msg.data[10] >= 0) {
			rInPhase = 256*msg.data[11]+msg.data[10]-256;
		} else {
			rInPhase = 256*msg.data[11]+msg.data[10]+256;
		}

		if((msg.data[13] < 0 && msg.data[12] <= 0) || (msg.data[13] >= 0 && msg.data[12] >= 0)){
			rQuad = 256*msg.data[13]+msg.data[12];
		} else if(msg.data[13] < 0 && msg.data[12] >= 0)
			rQuad = 256*msg.data[13]+msg.data[12]-256;
		else {
			rQuad = 256*msg.data[13]+msg.data[12]+256;
		}
//  ROS_INFO("%d %d %d %d", msg.data[9],msg.data[10],msg.data[11],msg.data[12]);
	}
	if(first) {
		first = false;
		for(int i = 0; i < BUFFER_SIZE; i++) {
			lQuadBuf[i] = lQuad;
			lInPhaseBuf[i] = lInPhase;
			rQuadBuf[i] = rQuad;
			rInPhaseBuf[i] = rInPhase;
		}
	}
	lQuadBuf[seq%BUFFER_SIZE] = lQuad;
	lInPhaseBuf[seq%BUFFER_SIZE] = lInPhase;
	rQuadBuf[seq%BUFFER_SIZE] = rQuad;
	rInPhaseBuf[seq%BUFFER_SIZE] = rInPhase;
	lQuad = 0;
	lInPhase = 0;
	rQuad = 0;
	rInPhase = 0;
	for(int i = 0; i < BUFFER_SIZE; i++) {
		lQuad += lQuadBuf[i];
		lInPhase += lInPhaseBuf[i];
		rQuad += rQuadBuf[i];
		rInPhase += rInPhaseBuf[i];
	}
	lQuad = lQuad / BUFFER_SIZE;
	lInPhase = lInPhase / BUFFER_SIZE;
	rQuad = rQuad / BUFFER_SIZE;
	rInPhase = rInPhase / BUFFER_SIZE;

	lMag = std::sqrt(std::pow(lInPhase,2)+std::pow(lQuad,2));
	lPhase = std::atan2(lInPhase, lQuad);

	rMag = std::sqrt(std::pow(rInPhase,2)+std::pow(rQuad,2));
	rPhase = std::atan2(rInPhase, rQuad);

	data.lData.resize(4);
	data.lData[0] = lInPhase;
	data.lData[1] = lQuad;
	data.lData[2] = lMag;
	data.lData[3] = lPhase;

	data.rData.resize(4);
	data.rData[0] = rInPhase;
	data.rData[1] = rQuad;
	data.rData[2] = rMag;
	data.rData[3] = rPhase;
//	ROS_INFO("%d %d %d %d %d %d", msg.data[4], msg.data[5], msg.data[6], msg.data[7], inPhase, quad);
	seq++;
	//write all data to a .txt file
//	string fileName ("/home/map34/efield_ws/src/pr2_pretouch_efield/src/datalog0_7_8.txt");
//	write_A_Row_To_File(inPhase, quad, mag, phase, fileName.c_str());
	pubPtr->publish(data);
}

int main(int argc, char** argv) {
	ros::init(argc, argv, "efield_publisher");
	ros::NodeHandle node;

	std::string side = argv[1];

	std::string subTopic = "raw_pressure/";
	if(!side.compare("right")) {
		subTopic+="r_gripper_motor";
	} else if(!side.compare("left")) {
		subTopic+="l_gripper_motor";
	} else {
		ROS_INFO("%s is not a valid topic", argv[1]);
    		ros::shutdown();
	}
	std::string pubTopic = "efield/"+side;
	ROS_INFO("Started publisher for the %s gripper", argv[1]);
	seq = 0;
	ts[0] = 0;
	ts[1] = 0;
	ts[2] = 0;
	ts[3] = 0;
	
	first = true;
	ros::Publisher pub = node.advertise<pr2_pretouch_efield::EField>(pubTopic,1000);
	pubPtr = &pub;
	ros::Subscriber sub = node.subscribe(subTopic, 1000, rawDataCallback); 
	ros::spin();
}
